import { Component } from "react";
import "./css/style.css";
import Addbox from "./components/addbox/addbox";
import AddList from "./components/addList/addList";
export default class App extends Component{
    constructor(props){
        super(props)
        let _self = this;
        _self.state ={
            comments:[
                {
                    userName:"李丹",
                    context:"真好啊"
                },
                {
                    userName:"朱庆倩",
                    context:"需要一个男朋友"
                }
            ]
        }
    }
    add =(item)=>{
        console.log(item)
        this.state.comments.unshift(item);
        this.setState({
            comments:this.state.comments
        })
    }
    delete=(index)=>{
        // 删除是在App进行删除的
        // 数据在App.jsx
        console.log(index)
        let comments = this.state.comments;
        comments.splice(index,1)
        this.setState({
            comments
        })
    }
    render(){
        return(
            <div className="box">
                <div className="Header">kw19月薪录入</div>
                <Addbox add={this.add}></Addbox>
                <AddList 
                comments={this.state.comments}
                delete={this.delete}
                ></AddList>
            </div>
        )
    }
}  