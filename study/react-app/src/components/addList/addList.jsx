import { Component } from "react";
import AddItem from "../addItem/addItem"
export default class Addbox extends Component{
    render(){
        console.log(this.props.comments);
        return(
            <div className="right userForm">
                <div>列表：</div>
                <ul>
                    {/* <li className="listBox">
                        <div>李丹：</div>
                        <div>内容  <button>删除</button></div>
                    </li> */}
                    {/* 循环父组件传过来的数据 渲染AddItem （<li>） */}
                    {
                        this.props.comments.map((comment , index)=>{
                          
                            return <AddItem 
                            delete={this.props.delete}
                            index={index}
                            comment={comment} 
                            key={index}></AddItem>
                        })
                    }
                    
                </ul>
            </div>
        )
    }
}