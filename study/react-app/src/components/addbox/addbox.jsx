import { Component } from "react";
export default class Addbox extends Component{
        constructor(props){
            super(props)
            let _self = this;
            _self.state ={
                userName:"",
                context:""
            }
        }
        changeUsername=(event)=>{
            this.setState({
                userName:event.target.value
            })
        }
        changeContent=(event)=>{
            this.setState({
                context:event.target.value
            })
        }
        addComment=()=>{
            // 根据输入的数据创建评论对象
            let {userName ,context} = this.state;
            let comment = {userName, context};
            // 打入父组件当中 
            this.props.add(comment)
        }
        render(){
            return(
                <div className="left userForm">
                    <div>
                        <span>姓名：</span>
                        <input 
                        type="text" 
                        value={this.state.userName}
                        onChange={this.changeUsername}
                        />
                    </div>
                    <div>
                        <span>内容：</span>
                        <textarea 
                        name="" 
                        id="" 
                        value={this.state.context}
                        onChange={this.changeContent}
                        cols="30" 
                        rows="10"
                        ></textarea>
                    </div>
                    <div>
                        <button onClick={this.addComment}>提交</button>
                    </div>
                </div>
            )
        }
        
}