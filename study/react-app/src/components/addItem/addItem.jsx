import { Component } from "react";
export default class AddItem extends Component{
    constructor(props){
        super(props)
        
    }
    deleteComment=()=>{
        // this.props.index 下标
        // this.props.delete 删除函数
        this.props.delete(this.props.index);
    }
    render(){
        console.log(this.props.comment)
        let comment = this.props.comment;
        return(
            <li className="listBox">
                <div>{comment.userName}：</div>
                <div>{comment.context}  <button onClick={this.deleteComment}>删除</button></div>
            </li>
        )
    }
}