import React,{Component} from "react";
// 为什么v-for wx:for 以及React的DOM必须要key 
// 为什么key值尽量不要是下标 
// 简单来说：当数组中的数据发生变化时：React 或者是v-for wx:for比较更新前后的元素key值
// 如果相同不同则删除之前的重新创建一个DOM元素
export default class MyComponent extends Component{
    constructor(props){
        super(props)
        this.state = {
            persons:[
                {
                    id:1,
                    name:"TOM",
                    age:12
                },
                {
                    id:2,
                    name:"JACK",
                    age:13
                }
            ]
        }

    }
    update=()=>{
        let persons = this.state.persons;
        persons.unshift({id:3,name:"Bob",age:14})
        this.setState({persons})
    }
    render(){
        let _self = this;
        const persons = _self.state.persons;
        let listItems = persons.map((person , index) =>
        // 循环组件和循环DOM需要key
            <ListItem key={person.id} {...person}/>
        )
        let listItems2= persons.map((person,index)=><ListItem key={index} {...person}/>)
        return (
            <div>
                <h2>使用id作为key</h2>
                <ul>{listItems}</ul>
                <h2>使用index作为key</h2>
                <ul>{listItems2}</ul>
                <button onClick={this.update}>首位添加一人（栈前追加）</button>
            </div>
        )
            
        }
    
}
function ListItem({id,name,age}){
    return <li>{id}--{name}--{age}---<input type="text"/></li>
}