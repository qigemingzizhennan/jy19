import React from 'react';//引入react
import ReactDOM from 'react-dom';//react-dom 
// ReactDOM.render(挂载的DOM ， 挂载到哪里)
// React里面使用是babel   在JS里面直接可以写标签 不用加引号 
// babel语法中直接写的标签是虚拟的DOM不是真实的DOM
// ReactDOM.render 将虚拟的DOM挂载到真实的DOM中
import App from "./App";
let person ={
  name:"TOM",
  sex:"女",
  age:"18"
}
// App子组件  向App组件传入数据  
ReactDOM.render(<App 
  {...person}
 
  />, document.getElementById("root"))