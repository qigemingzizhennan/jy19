import React,{Component} from "react";
export default class MyComponent extends Component{
    constructor(props){
        super(props)
        // 初始化实在 state
        this.state= {
            
        }
    }
    componentWillMount() {
        console.log("将要挂载")
    }
    componentDidMount(){"初始化已经挂载"}
    componentDidUpdate() {
        console.log("已经更新")
    }
    componentWillUnmount(){
        console.log("将要被移除")
    }
    
    handleBlur=(e)=>{
        let _self = this;
        // e.target e对象事件对象 target对象 当前的标签 
        console.log(e.target.value)
    }
    handleClick=()=>{
        let _self = this;
        console.log(this.msginput.value)
    }
    render(){
        return (
            <div>
                {/* ref  ref绑定标签 直接使用//入参指的就是当前ref绑定的标签*/}
                <input type="text" ref={input=> this.msginput = input}/>
                <button onClick={this.handleClick}>提示输入的数据</button>
                <input 
                type="text" 
                placeholder="失去焦点提示数据" 
                onBlur={this.handleBlur}
                />
            </div>
        )
    }
}