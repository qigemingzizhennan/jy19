import React,{Component} from "react";//引入react
// let = React:{
    // Component:
// }
// {Component} = React;
// React两种组件化形式 
// 1.工厂模式函数组件化（声明一个函数当做组件） 
// 2.类组件化（复杂组件） class声明的类当做组件
// export default function App(){//一个App函数
//     let myId = "IDgood";
//     // 标签绑定数据使用{写数据} 
//     // let msg = '我们一起学习React中的脚手架项目'
//     // React的循环DOM必须带key值
//     let  list= ["q","w","e","r"];
//     let  lis=[];
//     list.forEach((name,index)=> 
//       lis.push(
//         <span key={index}>
//             {name}
//         </span>
//       )
//     )
//     return <h1  id={myId.toUpperCase()}>{lis}</h1>
// }
// 
// class 实体类 类声明 可以写变量 可以写函数 
// class App 声明的类名 
// React组件化 React.Component 
// extends
// extends 父类引用的类为子类 子类可以继承父类的属性  因此可以将父类理解为原型对象的prototype对象
export default class App extends Component{
    // render 用于提供的容器参数 渲染成一个elementDOM对象 
   constructor(props){
    super(props)
    // 初始化状态
    this.state = {
        isLikeMe : true
    }
    console.log(this);
    // props 接收父组件传递来的参数
    console.log(this.props.name);
    console.log(this.props.age);
    console.log(this.props.sex)
    console.log(this.change)
    // bind返回的是一个函数
    this.change = this.change.bind(this);
   }
   change(){
    console.log("我被点了")
    
    this.state.isLikeMe = !this.state.isLikeMe;
    console.log(this.state.isLikeMe);
    // React 初始化状态改变页面不会改变 需要setState打回页面
    this.setState({
        isLikeMe:this.state.isLikeMe
    })
   }
    render(){ 
        console.log(this)//指向React对象
        // this.state.isLikeMe = !this.state.isLikeMe;
        const text = this.state.isLikeMe?"你喜欢我":"我喜欢你"
        // console.log(this.change);
        return <h2 onClick={this.change}>{text}</h2>
    }
}