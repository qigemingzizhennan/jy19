// pages/login/login.js
import {request} from "../../utils/https";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    logincaptcha:"",
    time:"验证码",
    timeDn:null,
    ischeck:false,//是否被点击
    phoneValuelogin:""
  },
  logincaptcha(e){
    let _self = this;
    _self.data.logincaptcha = e.detail.value;
  },
  loginphone(e){
    let _self = this;
    console.log(e.detail.value)
    _self.data.phoneValuelogin = e.detail.value;
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // /login/mobile
  },
  phoneBtn(){
    let _self = this;
    let data= {
      "account":_self.data.phoneValuelogin,"captcha":_self.data.logincaptcha,
      "spread":""}
    request(
      "/login/mobile",
      data,
      "POST",
      function(res){
        console.log(res)
        if(res.data.status == 200){
          wx.setStorageSync('token', res.data.data.token)
        //  返回上一个页面
        wx.navigateBack()
        }
      }
    )
  },
  verificationCode:function(){
    let _self = this;
    let tec = /^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$/;
    console.log(_self.data.phoneValuelogin)
    if(tec.test(_self.data.phoneValuelogin)){
      // /register/verify
      let data ={"phone":_self.data.phoneValuelogin,"type":"login"};
      
      request(
        "/register/verify",
        data,
        "POST",
        function(res){
          console.log(res)//res 里面 就有openid
          wx.showToast({
            icon:"none",
            title: res.data.msg
          })
        })
        
      _self.data.time = 60;
      _self.data.ischeck = true;
      _self.setData({
        ischeck:_self.data.ischeck
      })
  // 防抖节流
      _self.data.timeDn=setInterval(()=>{
        
        _self.setData({
          time:_self.data.time
        })
        if(_self.data.time ==0){
          _self.data.time = "验证码";
          _self.data.ischeck = false;
          _self.setData({
            time:_self.data.time,
            ischeck:_self.data.ischeck
          })
          clearInterval(_self.data.timeDn)
        }else{
          _self.data.time--;
        }
        
      },1000)
    }else{
      wx.showToast({
        title: '您输入的格式不对',
        icon:"none"
      })
    }
 
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})