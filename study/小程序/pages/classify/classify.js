// pages/search/search.js
import {request} from "../../utils/https"
Page({
  // 
  /**
   * 页面的初始数据
   */
  data: {
    classifyindex:0,
    isChecked:false,
    messageData:[],
    listRight:[],
    list:[
      // {name:"热门推荐"},
      // {name:"食品酒水"},
      // {name:"居家生活"},
      // {name:"苏宁生鲜"},
      // {name:"生活家电"},
      // {name:"苏宁国际"},
      // {name:"手机数码"},
      // {name:"大家电"},
      // {name:"电脑办公"},
      // {name:"奶粉尿裤"},
      // {name:"美妆洗护"},
      // {name:"珠宝首饰"},
      // {name:"厨卫电器"},
      // {name:"家装建材"},
      // {name:"运动户外"},
      // {name:"女装女鞋"},
      // {name:"男装男鞋"},
      // {name:"智能设备"},
      // {name:"内衣配饰"},
      // {name:"童装玩具"},
      // {name:"钟表眼镜"},
      // {name:"礼品乐器"},
      // {name:"皮具箱包"},
      // {name:"图书音像"},
      // {name:"苏宁极物"},
    ]
  },

  change(e){
    console.log(e)
    let _self = this;
    _self.data.classifyindex = e.currentTarget.dataset.classifyindex;
    // _self.data.messageData
    _self.data.listRight= [];
    for(let item of _self.data.list[_self.data.classifyindex].children){
      _self.data.listRight= [..._self.data.listRight,...item.children]
    }
    _self.setData({
      classifyindex:_self.data.classifyindex,
      listRight:_self.data.listRight
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
      console.log("监听页面加载")
      // 接收上一个页面发送过来的参数

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    let _self = this;
    console.log("监听页面初次渲染完成")
    // 一般来说用不到
    // 小程序打开一个页面时弹出红包等。。。下次再打开这个页面就没有
    // 只触发一次
    _self.searchClassify()
  },
  
  // 列表
  searchClassify(){
    let _self = this;
    request(
      "categories",
      {},
      "GET",
      function(res){
        // result
        console.log(res)
        for(let item of res.data.message){
          _self.data.list.push({
            name:item.cat_name,
            children:item.children
            // active:false
          })
        }
        // _self.data.list[0].active = true;
        if(_self.data.list.length>0){
          console.log(_self.data.list)
          for(let item of _self.data.list[0].children){
            _self.data.listRight= [..._self.data.listRight,...item.children]
          }
          // _self.data.listRight = _self.data.list[0].children;

        }
        console.log(_self.data.listRight)
        _self.setData({
          listRight:_self.data.listRight,
          list:_self.data.list
        })
      },
      function(err){
        // error
        console.log(err)
      }
    )
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    console.log("监听页面显示")
    // 初始化页面需要干啥都写在onshow 

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    console.log("监听页面隐藏")
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    console.log("监听页面卸载")
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})