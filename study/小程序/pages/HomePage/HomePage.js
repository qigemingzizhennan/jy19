import {
  request
} from "../../utils/https"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    List: [],
    ListTwo: [],
    messagegoods:[],
    pagenum:1
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    let _seft = this;
    _seft.homeShow();
    _seft.homeList();
  },
  homeShow() {
    let _seft = this;
    request(
      "getCanvas?terminal=3", {},
      "get",
      function (res) {
        _seft.data.List = JSON.parse(res.data.json);

        _seft.setData({
          List: _seft.data.List
        });
      }
    )
  },
  togoodsDetail(e){
    console.log(e.detail)
    wx.navigateTo({
      url: '/pages/shopDetail/shopDetail?id='+e.detail,
    })
  },
  homeList() {
    let _seft = this;
    request(
      "index", {},
      "get",
      function (res) {
        _seft.data.ListTwo = res.data.data;

        _seft.setData({
          ListTwo: _seft.data.ListTwo
        })
      }
    )
  },
  /**
   * 生命周期函数--监听页面显示
   */
   onShow() {
    // /goods/search
    let _self = this;
    _self.goodssearch();

  },
  goodssearch(){
    let _self = this;
    let data= {
      pagenum:_self.data.pagenum,
      pagesize:6
    }
    request(
      "goods/search",
      data,
      "get",
      function (res) {
        wx.stopPullDownRefresh();
        console.log(res.data.message.goods)
        _self.data.messagegoods = [..._self.data.messagegoods,...res.data.message.goods];
        _self.setData({
          messagegoods:_self.data.messagegoods
        })
      }
    )
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    console.log("1");
    let _self = this;
    wx.startPullDownRefresh()
    _self.data.pagenum =1;
    _self.data.messagegoods = [];
    _self.goodssearch()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    let _self = this;
    _self.data.pagenum++;
    _self.goodssearch()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  // 搜索框点击跳转
  Sarch() {
    wx.navigateTo({
      url: '/pages/HomeSecondaryPage/SarchPage/SarchPage',
    })
  },
  //导航栏菜单点击跳转
  navigationClick(e) {
    let {
      navigationindex
    } = e.currentTarget.dataset;
    switch (navigationindex) {
      case 0:
        wx.navigateTo({
          url: '/pages/HomeSecondaryPage/Allitems/Allitems',
        });
        break;
      case 1:
        wx.navigateTo({
          url: '/pages/HomeSecondaryPage/Ceefax/Ceefax',
        });
        break;
      case 2:
        wx.navigateTo({
          url: '/pages/HomeSecondaryPage/Collect/Collect',
        });
        break;
      case 3:
        wx.navigateTo({
          url: '/pages/HomeSecondaryPage/Coupon/Coupon',
        });
        break;
      case 4:
        wx.navigateTo({
          url: '/pages/HomeSecondaryPage/Collage/Collage',
        });
        break;
      case 5:
        wx.navigateTo({
          url: '/pages/HomeSecondaryPage/Integral/Integral',
        });
        break;
      case 6:
        wx.navigateTo({
          url: '/pages/HomeSecondaryPage/Seckill/Seckill',
        })
    }
  },
  //文字轮播点击跳转
  TextRotationClick() {
    wx.navigateTo({
      url: '/pages/HomeSecondaryPage/Ceefax/Ceefax',
    })
  },
  //优惠区图片点击跳转
  Image2Click() {
    wx.navigateTo({
      url: '/pages/HomeSecondaryPage/Allitems/Allitems',
    })
  },
  //优惠区图片点击跳转
  Image3Click() {
    wx.navigateTo({
      url: '/pages/HomeSecondaryPage/Ponints/Ponints',
    })
  },
  //为你推荐点击跳转
  recommendedClick(e) {
    let _seft = this;
    let {
      index
    } = e.currentTarget.dataset;
    let imageurl = _seft.data.ListTwo.likeInfo[index].image;
    let id = _seft.data.ListTwo.likeInfo[index].id;
    wx.navigateTo({
      url: '/pages/HomeSecondaryPage/ProductDetails/ProductDetails?id=' + id + '&' + 'image=' + imageurl,
    })

  }
})