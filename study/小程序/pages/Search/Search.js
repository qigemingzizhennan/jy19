// pages/Search/Search.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 初始化获取缓存可以在data
    // name:wx.getStorageSync("searchHis")//要获取缓存的名字
    name:[],
    time:"验证码"
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
   let _self = this;
  //  _self.data.name =  wx.getStorageSync('searchHis');//同步
    wx.getStorage({//异步
      key: 'searchHis',//获取的名字
      success (res) {
        console.log(res)
        _self.data.name = JSON.parse(res.data);
        _self.setData({
          name:_self.data.name
        })
      }
    })
   
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },
  searchData:function(e){
    let _self = this;
    _self.data.name.unshift(e.detail.value)
    // 异步
    wx.setStorage({
      key:"searchHis",//给当前存储的数据取个名字
      data:JSON.stringify([...new Set(_self.data.name)])//要存的数据
    })
    // 同步  给当前存储的数据取个名字 ， 要存的数据
    // wx.setStorageSync('searchHistory', e.detail.value)
    // 要存的数据只能是基本数据类型
  },
  removeHistory:function(){
    let _self = this;
    wx.removeStorage({
      key: 'searchHis',//缓存的名字
      success (res) {
        console.log(res)
        _self.data.name = [];
        _self.setData({
          name:_self.data.name
        })
      }
    })
  },
  verificationCode:function(){
    let _self  = this;
    
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})