// pages/myyigou/myyigou.js
import {request} from "../../utils/https";

Page({

  /**
   * 页面的初始数据
   */
  data: {
    name:"潘希林会的",
    islogin:wx.getStorageSync('token'),//用户没有登录
    list:[
      {
        name:"潘希林会的",
        age:"19"
      },
      {
        name:"周海波会的",
        age:"190"
      }
    ],
    nickName:"",
    avatarUrl:""
  },
  // 触发的函数写在data的同级
  clickme(e){
    // e对象 接收数据
    console.log(e)
    console.log(e.currentTarget.dataset.num)
    console.log("我被点了")
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  Userlogin:function(){
    let _self = this;
    wx.getUserProfile({
      lang: 'zh_CN',
      desc:"您确定登录授权吗？",
      success:(res)=>{
        let {
          encryptedData ,
          rawData,
          iv,
          signature,
          userInfo
         } = res;
        console.log(res)
         let {nickName ,avatarUrl} = userInfo;
         _self.data.avatarUrl = avatarUrl;
         _self.data.nickName = nickName;
         _self.setData({
           nickName:_self.data.nickName,
           avatarUrl:_self.data.avatarUrl
         })
         wx.login({
          success:(res)=>{
            console.log(res)
            let {code} = res;
            let data= {encryptedData ,rawData,iv,signature,code};
            request(
              "users/wxlogin",
              data,
              "POST",
              function(res){
                console.log(res)//res 里面 就有openid
              })
          }
         })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  goDetail(){
    wx.navigateTo({
      url: '/pages/shopDetail/shopDetail',
    })
  },
  handleTap(){
    console.log(1)
  },
  handleTap2(){
    console.log(2)
  },
  handleTap3(){
    console.log(3)
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let _self = this;
    // islogin:wx.getStorageSync('token')
    _self.data.islogin = wx.getStorageSync('token');
    _self.setData({
      islogin :_self.data.islogin
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})