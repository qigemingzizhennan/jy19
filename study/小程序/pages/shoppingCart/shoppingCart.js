// pages/myyigou/myyigou.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isshow:"hide",
    // edit
    isEdit:false,
    // 全选的选中状态
    checkall:false,
    allPrice:0,//总价 
    allNum:0,//数量
    // 店铺集合
    shopList:[
      // 店铺
      {
        isCheck:false,//
        logo:"/images/icon.png",//店铺图标
        shopName:"店铺一",
        getCoupon:"",//领券
        // 商品的集合
        goodsList:[
          // 商品
          {
            isCheck:false,
            goodsImage:"/images/goods1.png",//商品图片
            goodName:"商品介绍商品介绍商品介绍商品介绍商品介绍商品介绍商品介绍商品介绍商品介绍商品介绍商品介绍商品介绍商品介绍",
            merchandisePrice:"28.90",
            goodsinventory:10,// 存库  
            Maximumpurchase:9,//最大购买量
            num:"1"
          },
          {
            isCheck:false,
            goodsImage:"/images/goods2.png",//商品图片
            goodName:"洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水洒洒水",
            merchandisePrice:"11.90",
            num:"1",
            goodsinventory:9,// 存库
            Maximumpurchase:10,//最大购买量
          },
          {
            isCheck:false,
            goodsImage:"/images/tui-list1.png",//商品图片
            goodName:"驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁驱蚊器翁",
            merchandisePrice:"33.90",
            num:"1",
            goodsinventory:11,// 存库
            Maximumpurchase:10,//最大购买量
          }
        ]
      },
      // 店铺
      {
        isCheck:false,//
        logo:"/images/icon.png",//店铺图标
        shopName:"店铺而",
        getCoupon:"",//领券
        // 商品的集合
        goodsList:[
          // 商品
          {
            isCheck:false,
            goodsImage:"/images/goods2.png",//商品图片
            goodName:"阿萨德阿萨德阿萨德阿萨德阿萨德阿萨德阿萨德阿萨德阿萨德阿萨德阿萨德阿萨德阿萨德阿萨德阿萨德阿萨德阿萨德阿萨德阿萨德阿萨德阿萨德阿萨德阿萨德阿萨德阿萨德阿萨德阿萨德",
            merchandisePrice:"44.90",
            num:"1",
            goodsinventory:7,// 存库
            Maximumpurchase:8,//最大购买量
          },
          {
            isCheck:false,
            goodsImage:"/images/goods1.png",//商品图片
            goodName:"是自行车自行车是自行车自行车是自行车自行车是自行车自行车是自行车自行车是自行车自行车是自行车自行车是自行车自行车是自行车自行车是自行车自行车是自行车自行车是自行车自行车是自行车自行车是自行车自行车是自行车自行车是自行车自行车是自行车自行车是自行车自行车是自行车自行车是自行车自行车是自行车自行车",
            merchandisePrice:"55.90",
            num:"1",
            goodsinventory:8,// 存库
            Maximumpurchase:7,//最大购买量
          },
          {
            isCheck:false,
            goodsImage:"/images/tui-list2.png",//商品图片
            goodName:"求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多求大神多",
            merchandisePrice:"66.90",
            num:"1",
            goodsinventory:4// 存库

          }
        ]
      },
      {
        isCheck:false,//
        logo:"/images/icon.png",//店铺图标
        shopName:"店铺三",
        getCoupon:"",//领券
        // 商品的集合
        goodsList:[
          // 商品
          {
            isCheck:false,
            goodsImage:"/images/tui-goods1.png",//商品图片
            goodName:"嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷嗷",
            merchandisePrice:"144.90",
            num:"1",
            goodsinventory:3,// 存库
            Maximumpurchase:4,//最大购买量
          },
          {
            isCheck:false,
            goodsImage:"/images/tui-goods2.png",//商品图片
            goodName:"洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa洒洒水aa",
            merchandisePrice:"255.90",
            num:"2",
            goodsinventory:66,// 存库
            Maximumpurchase:7,//最大购买量
          },
          {
            isCheck:false,
            goodsImage:"/images/tui-list1.png",//商品图片
            goodName:"asdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasdaasda",
            merchandisePrice:"366.90",
            goodsinventory:2,// 存库
            Maximumpurchase:3,//最大购买量
            num:"1"
          }
        ]
      },
    ],

    // 店铺集合的拷贝
    shopListCopy:[],
    checkallCopy:false
  },
  // 关闭动画
  backGroundanimation(e){
    let _self = this;
    _self.data.isshow = "hide";
    _self.animate.translateY(350).step();
    _self.setData({
      isshow:_self.data.isshow,
      animationData:_self.animate.export()
    })
  },
  // 点击服务
  serviceFn(e){
    let _self = this;
    _self.data.isshow = "show";
    let animation = wx.createAnimation({
      
      timingFunction:"ease"
    })
    console.log(this)
    // this => 小程序的当前页面的实例化对象
    // animate 小程序当前页面的动画实例
    // 把创建的动画放心小程序的动画实例
    _self.animate = animation;
    animation.translateY(-350).step();
  
    _self.setData({
      isshow:_self.data.isshow,
      animationData:_self.animate.export()
    })
  },
  // 值失去焦点
  bindblur(e){
    let {shopindex , goodsindex} = e.currentTarget.dataset;
    let _self = this;
    let value = e.detail.value;
    if(value== "" ){
      _self.setData({
        shopList :_self.data.shopList
      })
    }else if(value == 0){
      _self.data.shopList[shopindex].goodsList[goodsindex].num = 1;
      _self.data.shopList[shopindex].goodsList[goodsindex].isCheck = true;
       // 判断店铺有没有选中
       _self.shopIsCheck(shopindex)
       // 判断全选有没有选中
       _self.shopCheckAll()
       // 计算总价
       _self.PriceNum()
       _self.setData({
         shopList:_self.data.shopList
       })
      _self.setData({
        shopList :_self.data.shopList
      })
    }
  },
  // 值改变时候
  bindinput(e){
    let _self = this;
    console.log(e)
    // e.detail.value
    let value = e.detail.value *1;
    let {shopindex , goodsindex} = e.currentTarget.dataset;
    console.log( value<=_self.data.shopList[shopindex].goodsList[goodsindex].goodsinventory )
    
    if(
      value>0
      &&
      value<199
      &&
      value<=_self.data.shopList[shopindex].goodsList[goodsindex].goodsinventory
      &&
      (_self.data.shopList[shopindex].goodsList[goodsindex].Maximumpurchase?value<_self.data.shopList[shopindex].goodsList[goodsindex].Maximumpurchase:true) ){
   
        // 正常用值
        _self.data.shopList[shopindex].goodsList[goodsindex].num = value;
      }else if(
        // 让值变成库存
        (_self.data.shopList[shopindex].goodsList[goodsindex].Maximumpurchase?_self.data.shopList[shopindex].goodsList[goodsindex].goodsinventory<=_self.data.shopList[shopindex].goodsList[goodsindex].Maximumpurchase:true)
        &&
        _self.data.shopList[shopindex].goodsList[goodsindex].goodsinventory<=value
        &&
        _self.data.shopList[shopindex].goodsList[goodsindex].goodsinventory<=199
      ){
        _self.data.shopList[shopindex].goodsList[goodsindex].num = _self.data.shopList[shopindex].goodsList[goodsindex].goodsinventory;
      }else if(
        _self.data.shopList[shopindex].goodsList[goodsindex].Maximumpurchase
        &&
        _self.data.shopList[shopindex].goodsList[goodsindex].Maximumpurchase<=_self.data.shopList[shopindex].goodsList[goodsindex].goodsinventory
        &&
        _self.data.shopList[shopindex].goodsList[goodsindex].Maximumpurchase<=199
        &&
        _self.data.shopList[shopindex].goodsList[goodsindex].Maximumpurchase<=value
      ){
        _self.data.shopList[shopindex].goodsList[goodsindex].num = _self.data.shopList[shopindex].goodsList[goodsindex].Maximumpurchase
      }else if(value>199){
        // 让数量等于
        // DecideWhichOneToGoinventory 函数的返回值 return (判断是不是用库存)
        // DecideWhichOneToGoMaximumpurchase （返回最大购买量）
        _self.data.shopList[shopindex].goodsList[goodsindex].num = _self.DecideWhichOneToGoinventory(
          _self.data.shopList[shopindex].goodsList[goodsindex].Maximumpurchase,
          _self.data.shopList[shopindex].goodsList[goodsindex].goodsinventory
        )
        ||
        _self.DecideWhichOneToGoMaximumpurchase(
          _self.data.shopList[shopindex].goodsList[goodsindex].Maximumpurchase,
          _self.data.shopList[shopindex].goodsList[goodsindex].goodsinventory
        )
        ||
        199
      //  
      }
      if(value){
        _self.data.shopList[shopindex].goodsList[goodsindex].isCheck= true;
       // 判断店铺有没有选中
        _self.shopIsCheck(shopindex)
        // 判断全选有没有选中
        _self.shopCheckAll()
        // 计算总价
        _self.PriceNum()
        _self.setData({
          shopList:_self.data.shopList
        })
      }
    // console.log(typeof(value))

  },
  // 判断是否用库存
  DecideWhichOneToGoinventory(a,b){
    if(a){
      if(b<199 && b<a){
        return b
      }else{
        return false
      }
    }else{
      if(b<199){
        return b
      }else{
        return false
      }
    }
  },
  // 判断最大购买量
  DecideWhichOneToGoMaximumpurchase(a,b){
    if(a){
      if(a<199&&b>a){
        return a
      }
    }else{
      return false
    }
  },
  // 删除
  deleteShop(){
    let _self = this;
    // 判断商品有没有选中
    let type = false;
    for(let item of _self.data.shopList){
      for(let items of item.goodsList){
        if(items.isCheck){
          type= true;
        }
      }
    }
    if(!type){
      wx.showToast({
        title: '请选择商品',
        icon:"none",
        duration:2000
      })
    }else{
      // 增删改查中删必须要有提示 除非是删除不调接口的数据
      // 欺骗用户视觉
      // 前端的静态删除 如果循环删除数组中的值 那么需要逆着删 倒着删
      wx.showModal({
        title: '提示',
        content: '是否确定删除',
        cancelText:"确定",
        cancelColor:"#f00",
        confirmText:"取消",
        confirmColor:"#333",
        success (res) {
          if (res.confirm) {
            // console.log('用户点击确定')

          } else if (res.cancel) {
            // console.log('用户点击取消')
            for(let index = _self.data.shopList.length-1;index>=0;index--){
              if(_self.data.shopList[index].isCheck){
                    _self.data.shopList.splice(index,1);
                    _self.data.shopListCopy.splice(index,1)
              }else{
                for(let indexx = _self.data.shopList[index].goodsList.length-1;indexx>=0;indexx--){
                  if(_self.data.shopList[index].goodsList[indexx].isCheck){
                    _self.data.shopList[index].goodsList.splice(indexx,1);
                    _self.data.shopListCopy[index].goodsList.splice(indexx,1)
                  }
                }
            }
          }
          _self.data.isEdit = !_self.data.isEdit;

            // for(let index in _self.data.shopList){
            //   if(_self.data.shopList[index].isCheck){
            //     _self.data.shopList.splice(index,1)
            //   }else{
            //     for(let indexx in _self.data.shopList[index].goodsList){
            //       if(_self.data.shopList[index].goodsList[indexx].isCheck){
            //         _self.data.shopList[index].goodsList.splice(indexx,1)
            //       }
            //     }
            //   }
            // }
            _self.setData({
              shopList:_self.data.shopListCopy,
              isEdit:_self.data.isEdit,
              checkall:_self.data.checkallCopy
            })
            // 计算价格
            _self.PriceNum()
          }
        }
      })
    }
  },
  // 编辑
  toEdit(){
    let _self = this;
    if(!_self.data.isEdit){
      _self.data.shopListCopy = JSON.parse(JSON.stringify(_self.data.shopList));//深拷贝
      _self.data.checkallCopy = _self.data.checkall;
      for(let item of _self.data.shopList){
        item.isCheck = false;
        for(let items of item.goodsList){
          items.isCheck = false
        }
      }
      _self.data.checkall = false;
      _self.setData({
        shopList:_self.data.shopList,
        checkall:_self.data.checkall
      })

    }else{
      _self.setData({
        shopList:_self.data.shopListCopy,
        checkall:_self.data.checkallCopy
      })
    }

    _self.data.isEdit = !_self.data.isEdit;
    // 计算总价
    _self.PriceNum()
    _self.setData({
      isEdit:_self.data.isEdit
    })
  },
  // 加号
  plusFn(e){
    let _self = this;
    // let goodsindex = e.currentTarget.dataset.goodsindex;
    // let shopindex = e.currentTarget.dataset.shopindex;
    let {shopindex , goodsindex} = e.currentTarget.dataset;
    // goodsinventory Maximumpurchase
    // 判断库存是否小于数量
    // goodsinventory Maximumpurchase
    if(
      _self.data.shopList[shopindex].goodsList[goodsindex].goodsinventory<(_self.data.shopList[shopindex].goodsList[goodsindex].Maximumpurchase?_self.data.shopList[shopindex].goodsList[goodsindex].Maximumpurchase:999999999999999999999999999999999)){
        console.log(_self.data.shopList[shopindex].goodsList[goodsindex].goodsinventory<_self.data.shopList[shopindex].goodsList[goodsindex].Maximumpurchase)
        console.log(_self.data.shopList[shopindex].goodsList[goodsindex].goodsinventory);
        console.log(_self.data.shopList[shopindex].goodsList[goodsindex].Maximumpurchase)
     
          if(_self.data.shopList[shopindex].goodsList[goodsindex].goodsinventory>_self.data.shopList[shopindex].goodsList[goodsindex].num){
            _self.data.shopList[shopindex].goodsList[goodsindex].num++;
            _self.data.shopList[shopindex].goodsList[goodsindex].isCheck = true;
          }
      }
      if(_self.data.shopList[shopindex].goodsList[goodsindex].Maximumpurchase && _self.data.shopList[shopindex].goodsList[goodsindex].Maximumpurchase<_self.data.shopList[shopindex].goodsList[goodsindex].goodsinventory){
        
        if(_self.data.shopList[shopindex].goodsList[goodsindex].Maximumpurchase>_self.data.shopList[shopindex].goodsList[goodsindex].num){
        
          _self.data.shopList[shopindex].goodsList[goodsindex].num++;
          _self.data.shopList[shopindex].goodsList[goodsindex].isCheck = true;
        }
      }

    // _self.data.shopList[shopindex].goodsList[goodsindex].num ++;
    // 判断店铺有没有选中
    _self.shopIsCheck(shopindex)
    // 判断全选有没有选中
    _self.shopCheckAll()
    // 计算总价
    _self.PriceNum()
    console.log(_self.data.shopList);
    _self.setData({
      shopList:_self.data.shopList
    })
  },
  // 减号
  minusFn(e){
    let _self = this;
    // let goodsindex = e.currentTarget.dataset.goodsindex;
    // let shopindex = e.currentTarget.dataset.shopindex;
    let {shopindex , goodsindex} = e.currentTarget.dataset;
    if(_self.data.shopList[shopindex].goodsList[goodsindex].num>1){
        _self.data.shopList[shopindex].goodsList[goodsindex].num--;
        _self.data.shopList[shopindex].goodsList[goodsindex].isCheck = true;
        // 判断店铺有没有选中
        _self.shopIsCheck(shopindex)
        // 判断全选有没有选中
        _self.shopCheckAll()
        // 计算总价
        _self.PriceNum()
        console.log(_self.data.shopList);
        _self.setData({
          shopList:_self.data.shopList
        })
    }
  },
  // 计算总价 allPrice allNum
  PriceNum(){
    let _self = this;
    let Price = 0;
    let num = 0;
    for(let item of _self.data.shopList){
      for(let goodsitem of item.goodsList){
       if(goodsitem.isCheck){
        Price +=(goodsitem.num*1)*(goodsitem.merchandisePrice*1);
        num+=(goodsitem.num*1)
       }
      }
    }
    console.log(Price);
    console.log(num);
    _self.data.allPrice = Price.toFixed(2);
    _self.data.allNum = num;
    _self.setData({
      allNum:_self.data.allNum,
      allPrice:_self.data.allPrice
    })
  },
  // 店铺的选中
  shopIsCheck(shopindex){
    let _self = this;
    let type = false;
    for(let item of _self.data.shopList[shopindex].goodsList){
      if(!item.isCheck){
        type = true
      }
    }
    if(!type){
      _self.data.shopList[shopindex].isCheck = true;
    }else{
      _self.data.shopList[shopindex].isCheck = false;
    }
  },
  // 全选的选中
  shopCheckAll(){
    let _self = this;
    let type = true;
    for(let item of _self.data.shopList){
      if(!item.isCheck){
        type= false
      }
    }
    _self.data.checkall = type;
    _self.setData({
      checkall:_self.data.checkall
    })
    
  },
  // 商品选中
  goodscheck(e){
    let _self = this;
    // let goodsindex = e.currentTarget.dataset.goodsindex;
    // let shopindex = e.currentTarget.dataset.shopindex;
    let {shopindex , goodsindex} = e.currentTarget.dataset;
    _self.data.shopList[shopindex].goodsList[goodsindex].isCheck = !_self.data.shopList[shopindex].goodsList[goodsindex].isCheck;
     // 判断店铺有没有选中
     _self.shopIsCheck(shopindex)
     // 判断全选有没有选中
     _self.shopCheckAll()
     // 计算总价
     _self.PriceNum()
     console.log(_self.data.shopList);
     _self.setData({
       shopList:_self.data.shopList
     })
  },
  // 店铺的选中
  shopcheck(e){
    let _self = this;
    let {shopindex} = e.currentTarget.dataset;
    _self.data.shopList[shopindex].isCheck = !_self.data.shopList[shopindex].isCheck;
    for(let item of _self.data.shopList[shopindex].goodsList){
      item.isCheck = _self.data.shopList[shopindex].isCheck
    }
    // 判断全选有没有选中
    _self.shopCheckAll()
    // 计算总价
    _self.PriceNum()
    console.log(_self.data.shopList);
    _self.setData({
      shopList:_self.data.shopList
    })
  },
  // 全选
  checkalltype(){
    let _self = this;
    _self.data.checkall = !_self.data.checkall;
    for(let item of _self.data.shopList){
      item.isCheck = _self.data.checkall;
      for(let itemgoods of item.goodsList){
        itemgoods.isCheck = _self.data.checkall;
      }
    }
    _self.PriceNum()
    _self.setData({
      shopList:_self.data.shopList,
      checkall:_self.data.checkall
    })
  },
  ToSettle:function(){
    let _self = this;
    let a = 1;
    let b =2;
    // 如果传多个参数 不是一个
    // 我们可以以对象传参
    // let obj = {
    //   a,b
    // }
    wx.setStorageSync('shoplist', JSON.stringify(_self.data.shopList))
    wx.navigateTo({
      // url: '/pages/finalStatement/finalStatement?obj='+JSON.stringify(_self.data.shopList),
    
      url: '/pages/finalStatement/finalStatement'
      // 小程序的页面传参
      // 页面跳转地址?参数名1=参数值1&参数名2=参数值2
      // 页面传参如果传的数据太多
      // 不要拼接传 有可能导致传的不全
      

    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})