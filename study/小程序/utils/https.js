/*
  调接口在公司里面必然走的时候封装
  我们不一定看得懂别人怎么封装的 但是一定要会用别人封装的
  1）开发环境：代码正在开发 dev development 
  2) 测试环境：代码都写完 提交给测试测  test环境
  3）预生产环境：灰度环境  
  // www.baidu.com 
  // www.baidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidubaidu.com
  4)生产环境 :线上环境 master环境 production环境 （明天上生产）
  3.在公司里面不要看别人封装的代码 先会用再说

    https://api-hmugo-web.itheima.net/api/public/v1/
    https://api-hmugo-web.itheima.net//域名
    api/public/v1/ 

*/ 
// https://wxapi.yixiang.co/api/register/verify
// let configurl="https://wxapi.yixiang.co";//域名
let configurl="https://api-hmugo-web.itheima.net";

let baseurl = {
  // /api/public/v1/ 目录
  materUrl:"/api/public/v1/",
  // materUrl:"/api",//主目录
  ThethreeUrl:"第三方接口目录",//导航 身份号验证 广告
  uploadUrl:"上传接口目录",//上传
  downloadUrl:"下载接口地址"
}
// https://api-hmugo-web.itheima.net/api/public/v1/goods/qsearch
// configurl +baseurl.materUrl +"goods/qsearch"
/*
  接口调用的函数封装 (语义化)
  *模块的入参
  *url 服务器的地址
  *data 请求的参数
  *method 请求的方式
  *请求成功的回调函数
  *请求失败的回调函数
  *该接口是否需要登录 
*/ 
function request(
  url,
  data={},
  method="get",
  doSuccess,
  dofail=function(){},
  islogin = false
){
  // https://api-hmugo-web.itheima.net/api/public/v1/categories
  // 如果接口需要登录并且用户没有登录我们用户先登录
  // 判断Token 报文 前端发送给后端数据后返回的文本
  // Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjI3ODQ1LCJ1TmFtZSI6IjEzODEzOTg4ODg2Iiwic2NvcGUiOjgsImV4cCI6MTY3ODMwODM0NCwiaWF0IjoxNjc4MjM2MzQ0fQ.e4VvQ7NR7NiiarbFX13CYbI5o9CQ089l3fmF4xTn9EU
  // 如果需要登录 但用户没有登录 那就去登录
  if(islogin && !wx.getStorageSync('token')){
    wx.navigateTo({
      url: '/pages/login/login',
    })
  }
  // 如果用户已经登录那么就在头部里面添加token
  let header = {
    "Authorization" :`Bearer ${wx.getStorageSync('token')}`
  }
  wx.request({
    url: `${configurl}${baseurl.materUrl}${url}`,//地址
    data,
    header,
    method,
    success:res=>{
      // res返回的数据
      doSuccess(res)
    },
    fail:err=>{
      dofail(err)
    }

  })
}
// module.exports 模块化抛出 该JS文件中所有函数可以模块化 好处在于 使用该JS文件某个函数时其他的函数不被读取
// 先画页面 后调接口
module.exports = { request }
