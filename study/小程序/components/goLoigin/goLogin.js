// components/goLoigin/goLogin.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 组件事件触发的函数写在methods里面
    goLogin(){
      wx.navigateTo({
        url: '/pages/login/login',
      })
    }
  }
})
