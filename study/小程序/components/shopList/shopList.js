// components/shopList/shopList.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    // 接收传入的数据
    // 父页面传给组件的数据在这里定义
    messagegoods:{
      type:Array//传入的数据的类型
    },
    typeName:{
      type:String//字符串
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 在小程序组件当中 事件写在methods里面
    recommendedClick(e){
      console.log(e.currentTarget.dataset.goods_id);
      let {goods_id} = e.currentTarget.dataset;
      // 进入商品详情页由页面触发
      // wx.navigateTo({
      //   url: '/pages/shopDetail/shopDetail',
      // })
      // 组件触发事件后通知页面触发事件
      let _self = this;
      // this.triggerEvent(让页面接收的名字，发送给页面的参数)
      _self.triggerEvent("togoodsDetail" , goods_id)

    }
  }
})
