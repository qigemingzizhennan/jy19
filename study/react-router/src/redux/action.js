// 为了描述仓库的行为对象
// 状态变化行为 操作行为
import { InMinute, Inplus} from "./actions-type"
export const inPlus = number=>({
    type: Inplus,//行为标识
    number:number,//数据类型：数据
})
export const inMinute = number=>({
    type: InMinute,//行为标识
    number:number,//数据类型：数据
})