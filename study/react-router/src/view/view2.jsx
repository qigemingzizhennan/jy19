import { Component } from "react";
import {Route , NavLink , Redirect} from "react-router-dom";
import View3 from "./view3";
import View4 from "./view4";
export default class View1 extends Component{
    render(){
        return(
            <div>
                <div>页面2</div>
                {/* Vue的三级路由 React的二级路由 多级时要加父级路由名 */}
                <NavLink to="/view2/view3">去页面3</NavLink>
                <NavLink to="/view2/view4">去页面4</NavLink>
                <div>
                    <Route path="/view2/view3" component={View3}></Route>
                    <Route path="/view2/view4" component={View4}></Route>
                </div>
                <div>
                    <Redirect to="/view2/view4"></Redirect>
                </div>
            </div>
        )
    }
}