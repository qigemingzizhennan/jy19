import React from 'react';//引入react
import ReactDOM from 'react-dom';//react-dom 
import App from "./app.jsx"
// BorserRouter(history) 确定当前路由的模式
// hashRouter(hash) 
import {couter} from "./redux/reducer";
// 在redux中去取出创建仓库的方法 createStore
import {createStore} from "redux";
import {BrowserRouter} from "react-router-dom";
// 将管理者couter文件注册到仓库当中
let store = createStore(couter);
// 因redux不像vuex一样是 框架自带 而是一个独立的所以我们不需要 让redux监听react


let render= ()=>{
    ReactDOM.render(
        <BrowserRouter>
            <App
                store={store}
            />
        </BrowserRouter>,
        document.getElementById("root")
    )
}
render()
// 仓库监听react
store.subscribe(render);