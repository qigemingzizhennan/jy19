import { Component } from "react";
import { NavLink, Route, Redirect } from "react-router-dom";
import View1 from "./view/view1";
import View2 from "./view/view2";
import * as actions from "./redux/action"
export default class App extends Component{
    // constructor(props){
    //     super(props)
    //     let _self = this;
    //     _self.state = {

    //     }
    // }
    state={
        count:1
    }
    render(){
        let _self = this;
        return(
            <div>
                {/* NavLink 跳转路由按钮 */}
                <div>
                    {/* to 跳转的路由的地址名 */}
                    <NavLink to="/view1">去页面1</NavLink>
                    <NavLink to="/view2">去页面2</NavLink>
                </div>
                <div>
                    {/* Route 路由的地址 路由占位
                        path 设置路由跳转的地址名
                        component 引入的地址
                    */}
                    <div>2</div>

                    <Route path="/view1" component={View1}></Route>
                    <Route path="/view2" component={View2}></Route>
                    <div>1</div>
                    <Redirect to="/view1"></Redirect>
                    {/* Redirect 路由重定向 默认打开哪个页面 */}
                    {/* store.getState() redux里面初始变量 */}
                    <div>这里显示一个数字 {this.props.store.getState()}</div>
                    <div>
                        <span>这个数字一次性加多少</span>
                        <select name="" id="" ref="numSelect">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                    </div>
                    <div>
                        <button onClick={this.inPlus}>+</button>
                        <button onClick={this.inMinute}>-</button>
                    </div>
                </div>
            </div>
        )
    }
    inPlus=()=>{
        let _self = this;
        let num = _self.refs.numSelect.value *1;
        // let count = _self.state.count+num;
        // _self.setState({count})
        _self.props.store.dispatch(actions.inPlus(num))//dispatch => actions
    }
    inMinute=()=>{
        let _self = this;
        let num = _self.refs.numSelect.value *1;
        // let count = _self.state.count-num;
        _self.props.store.dispatch(actions.inMinute(num))
        // _self.setState({count})
    }
}