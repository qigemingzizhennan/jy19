
import axios , {InternalAxiosRequestConfig , AxiosResponse} from "axios";
import {getToken} from "@/utils/auth"

axios.interceptors.request.use(
    (config:InternalAxiosRequestConfig) =>{
        if(getToken("token")){
            config.headers.Authorization = getToken("token")
        }
        return config
    }
),
axios.interceptors.response.use(
    (response:AxiosResponse)=>{
        return response
    }
)
export default axios
