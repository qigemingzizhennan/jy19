export interface menusData{
    page:string,
    size:string,
    blurry?:string,
    createTime?:string,
    endTime?:string
}
export interface apimenusData{
    "id":Object,
    "name":String,
    "sort":Number,
    "path":String,
    "component":Object,
    "componentName":Object,
    "iframe":Boolean,
    "roles":Array<number>,
    "pid":Number,
    "icon":String,
    "cache":Boolean,
    "hidden":String,
    "type":Number,
    "permission":null
}