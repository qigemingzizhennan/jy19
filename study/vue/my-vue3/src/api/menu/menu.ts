import request from "@/utils/request";
import { AxiosPromise } from "axios";
// /api/menus?page=0&size=10&sort=id%2Cdesc
// 用户名 密码 验证码 uuid
// const yxStoreProduct = (data)=>request({})
import {menusData ,apimenusData} from "./index";
export function menusApi(data:menusData):AxiosPromise{
    console.log(data)
    return request({
        url:`/api/api/menus?page=${data.page}&size=${data.size}&sort=id,desc${data.blurry?"&blurry="+data.blurry:''}${data.createTime?"&createTime="+data.createTime+"&createTime="+data.endTime:""}`,
        // params:data,
        method:"GET"
    })
}
// 整理菜单接口
export function menustree():AxiosPromise{
    return request({
        url:`/api/api/menus/tree`,
        method:"GET"
    })
}
// 增菜单 /api/menus
export function apimenus(data:apimenusData):AxiosPromise{
    return request({
        url:`/api/api/menus`,
        method:"POST",
        data
    })
}
// http://47.100.203.148:8000
// http://47.100.203.148:8000/api/menus?page=0&size=10&sort=id%2Cdesc

