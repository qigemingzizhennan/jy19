// 登录
export interface LoginData{
    username:string,
    password:string,
    code:string,
    uuid:string
}
// 验证码
export interface VerifyCode{
    uuid:string,
    code:string
} 