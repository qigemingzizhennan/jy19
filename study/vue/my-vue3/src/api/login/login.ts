import request from "@/utils/request";
import { AxiosPromise } from "axios";
// 用户名 密码 验证码 uuid
// const yxStoreProduct = (data)=>request({})
import {LoginData , VerifyCode} from "./index";
export function loginApi(data:LoginData):AxiosPromise{
    return request({
        url:"/api/auth/login",
        method:"post",
        data
    })
}
export function codeApi():AxiosPromise<VerifyCode>{
    
    return request({
        url:"/api/auth/code",
        method:"get"
    })
}
// http://47.100.203.148:8000/auth/code
// http://47.100.203.148:8000/auth/login
