import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import login from '../views/login/login.vue'
import home from "../views/home/home.vue"
const routes: Array<RouteRecordRaw> = [
  {
    path:"/",
    component:login,
    meta:{},
  },
  {
    path:"/home",
    component:home,
    meta:{

    },
    redirect:"/index",
    children:[
      {
        path:"/index",
        component:()=>import(`@/views/index/index.vue`)
      },
      {
        path:"/ComponentTransferValue",
        component:()=>import(`@/views/ComponentTransferValue/index.vue`)
      }
    ]
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
