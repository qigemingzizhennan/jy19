import { createApp } from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import ElementPlus, { ElTable } from 'element-plus';
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import 'element-plus/theme-chalk/index.css';
import {setupStore}  from "@/store";
import mitt from "mitt";
const app = createApp(App)//创建vue实例化对象
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
const debounce = (fn:any, delay:any) => {
  let timer:any = null;
  return function () {
    // let context = this;
    let args = arguments;
    clearTimeout(timer);
    timer = setTimeout(()=>{
      // fn.apply(this, args);
    }, delay);
  }
}

const _ResizeObserver = window.ResizeObserver;
window.ResizeObserver = class ResizeObserver extends _ResizeObserver{
  constructor(callback:any) {
    callback = debounce(callback, 16);
    super(callback);
  }
}

setupStore(app)
// 3.0移除filter {{ | }} computed
// 全局过滤器
app.config.globalProperties.$filters = {
  // currencyUSD 过滤器的名字(入参){return 处理}
  currencyUSD(value:any) {
    var date = new Date(value );
    var Y = date.getFullYear() + "-";
    var M =
      (date.getMonth() + 1 < 10
        ? "0" + (date.getMonth() + 1)
        : date.getMonth() + 1) + "-";
    var D = (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + " ";
    var h = date.getHours() + ":";
    var m = date.getMinutes() + ":";
    var s = date.getSeconds();
    return Y + M + D + h + m + s;

  }
}
// TS 后缀文件全称是typeScript  JavaScript的超集 
// TS只是JavaScript的拓展工具 所有任何TS文件里面都可以正常写任何的JS 
// TS+vue3.0
app.use(ElementPlus).use(router).mount('#app')
app.config.globalProperties.$bus = new (mitt as any)();
// new 构造函数 声明类（） new(构造函数名 as 类型)()