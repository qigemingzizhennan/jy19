// 用户的状态管理
import {defineStore} from "pinia";//3.0 vuex  pinia
import {loginApi } from "@/api/login/login";//登录接口
import {LoginData} from "@/api/login/index";//TS
import {setToken} from "@/utils/auth"
// defineStore(该仓库的名字 ,仓库处理的对象)
// 2.0 里面vuex调后端的接口使用dispatch 
// 正常情况我们是vuex dispatch最多
export const UserStore = defineStore("user",()=>{
    // 登录
    // let token:string = ""
    // loginData 登录传入的数据 
    function Storelogin(loginData:LoginData){
        // 执行的结果返回给页面
        return new Promise((reslove , reject)=>{
            loginApi(loginData).then(res=>{
               let {token} = res.data;
                setToken("token" , token)
                // 缓存当中 
                // 用户信息 token 存在Cookie或者sessionStorage
                // 用户关闭窗口再次打开就重新登录sessionStorage
                // 返之用cookie
                // 不用用户登录后在相同页面显示不同的操作按钮
                /*
                    登录接口返回 id 
                    1 => 管理员
                    2 => 普通员工
                    3 => 游客
                */ 
                //后端返回的id存在缓存中
                //如：人员管理 新增按钮只能管理员新增其他人新增看不到
                // 再当前页面获取缓存id值 
                // 再用if 或者v-if进行判断显示 或者禁用
            //    
                reslove(res)//Promise 成功回调
            }).catch(error=>{
                reject(error);
            })
        })
    }
    return {
        Storelogin
    }
})