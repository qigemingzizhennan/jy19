const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer:{
    // 代理转发 本地反向代理解决浏览器跨域的限制
    proxy:{
      "/api":{//自定义 是自行设置请求的前缀 ，按照这个来匹配请求 有这个名字才能开启代理
        target:"http://47.100.203.148:8000",//解决跨域的服务器地址  目标域名 会自动替换掉匹配字段之前的路径,
        ws:"",//是否启用websocket 即时通讯(实时聊天 实时根据后端数据的变化进行变化)
        changeOrgin:true,//是否跨域
        pathRewrite:{
          // 重写匹配的字段（一般请求下就是设置的请求前缀）
          "/api":""
        }
      }
    }
  }
})
