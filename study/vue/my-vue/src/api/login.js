// api 每一个JS文件都是每个模块的接口的集合
/*
    前端：小智
    后端：小刚
    修改时间：2023年3月22日
*/ 
// 引入我们需要的环境地址
import { businessUrl } from "../config/env";
// 引入aioxs 中的request方法 
import request from "../config/axios";
// 调用验证码接口 auth/code= ( 入参 )=>
export const authcode = ()=>request({
    url:`${businessUrl}auth/code`,//接口地址
    method:"GET",
    // data:{} 发送给后端的数据
})
// 登录 auth/login
export const authlogin = (data)=>request({
    url:`${businessUrl}auth/login`,
    method:"POST",
    data
})
// 菜单 /menus/build
export const menusbuild = ()=>request({
    url:`${businessUrl}api/menus/build`,
    method:"GET"
})
