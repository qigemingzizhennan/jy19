// http://47.100.203.148:8000/api/yxStoreProduct?page=0&size=10&sort=id%2Cdesc&isShow=1&isDel=0
// 商品管理
/*
    前端：小智
    后端：小刚
    修改时间：2023年4月1日
*/ 
// 引入我们需要的环境地址
import { businessUrl } from "../config/env";
// 引入aioxs 中的request方法 
import request from "../config/axios";
// 调用验证码接口 auth/code= ( 入参 )=>
export const yxStoreProduct = (data)=>request({
    url:`${businessUrl}api/yxStoreProduct?page=${data.page}&size=${data.size}&sort=id%2Cdesc&isShow=${data.isShow}&isDel=0&value=${data.value}&${data.storeName?"storeName="+data.storeName:''}`,//接口地址
    method:"GET",
    // data:{} 发送给后端的数据
})
// 分类下拉
// http://47.100.203.148:8000/api/yxStoreCategory?enabled=true
export const yxStoreCategory = ()=>request({
    url:`${businessUrl}api/yxStoreCategory?enabled=true`,
    method:"GET"
})
// /api/materialgroup/page?total=0&page=1&size=100&sort=create_time,desc
export const materialgrouppage = ()=>request({
    url:`${businessUrl}api/materialgroup/page?total=0&page=1&size=99999&sort=create_time,desc`,
    method:"GET"
})
// /api/materialgroup
export const materialgroupadd =(data)=>request({
    url:`${businessUrl}api/materialgroup`,
    method:"POST",
    data
})
// 上传图片 /api/upload
export const apiupload = (data)=>request({
    url:`${businessUrl}api/upload`,
    method:"POST",
    data
})
// 上传图片 api/material
export const apimaterial = (data)=>request({
    url:`${businessUrl}api/material`,
    method:"POST",
    data
})
// 弹窗右边的数据
// /api/material/page?page=0&size=12&descs=create_time&sort=create_time,desc
export const apimaterialpage = (data)=>request({
    url:`${businessUrl}api/material/page?page=${data.page}&size=${data.size}&descs=create_time&sort=create_time,desc${data.groupId!='0'?'&groupId='+data.groupId:''}`,
})
// 重命名 /api/materialgroup
export const apimaterialgroup = (data)=>request({
    url:`${businessUrl}api/materialgroup`,
    method:"put",
    data
})
// 删除分组 /api/materialgroup/e19ee3a469048f6c932f7a83988fffd9
export const apimaterialgroupDELETE = (data)=>request({
    url:`${businessUrl}api/materialgroup/${data.id}`,
    method:"DELETE"
})
// http://47.100.203.148:8000/api/yxStoreProduct 新增
export const apiyxStoreProduct = (data)=>request({
    url:`${businessUrl}api/yxStoreProduct`,
    method:"POST",
    data
})
// /api/yxStoreProduct/isFormatAttr/11
// data:{id ,data}
export const apiyxStoreProductisFormatAttr = ({id ,data})=>request({
    url:`${businessUrl}api/yxStoreProduct/isFormatAttr/${id}`,
    method:"POST",
    data
})
// http://47.100.203.148:8000/api/yxStoreProduct/setAttr
export const yxStoreProductsetAttr  = ({id,data})=>request({
    url:`${businessUrl}api/yxStoreProduct/setAttr/${id}`,
    method:"POST",
    data
})