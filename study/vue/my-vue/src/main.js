// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.config.productionTip = false
Vue.use(ElementUI);
// 引入我们指令的JS的文件 
import "./config/directive";
import md5 from "md5";//引入下载的md5
// 怎么给Vue全局注册一个变量 （怎么给Vue原型注册一个属性） Vue.prototype
Vue.prototype.$md5 = md5;
/* eslint-disable no-new */
// new Vue 创建vue实例化对象 new Vue() 
// 全局组件
// 一般来说全局组件 只用Vue的插件安装 引入的外部组件 并且任何的Vue文件都能使用
// 引入插件 （百度上的实例） 插件（多个页面都需要这个插件）
// 全局组件注册成功就会所有的页面直接使用
// 1.全局组件  Vue.component
// 2.父子组件 父传子是props 子传父$emit 
// 3.兄弟组件 应用场景少 传值$emit 接值$on
// 4.混合混入组件 应用场景无 所有的组件的共享 用不到的组件也会共享 
import VueBarcode from "vue-barcode";
import { quillEditor } from 'vue-quill-editor'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css';
Vue.component("quillEditor", quillEditor);
// Vue.component 全局注册 （自定义的全局组件的名字 ， 引入的插件）
Vue.component("barcode", VueBarcode);
import * as echarts from 'echarts';//将echarts插件里面的*所有东西都赋予给echarts
// 把echarts注册成vue的全局变量
Vue.prototype.$echarts = echarts;//this.$echarts

// 引入全局mixin Vue.mixin
import {hunhe} from "./config/mixin";
Vue.mixin(hunhe);//让其混合使用
//vue文件有生命周期的定义 混合也有 二者没有冲突
// 引入vuex的对象 引入仓库
import store from './vuex/store';
console.log(store)
// vue3.0 对应的vuex仓库是vuex4+ 
// vue2.0 对应的vuex仓库是vuex3+
new Vue({
  el: '#app',//element getElementBy...() 
  router,//路由 <a></a>
  components: { App },//组件：{App:App}
  template: '<App/>',//模板："<App></App>"
  store//在vue实例化对象里面引入仓库
})
