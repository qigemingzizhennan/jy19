// 项目状态管理仓库
/* 
    什么是Vuex?
    Vuex是Vue共享状态的集中式管理工具
    Vuex中Store是什么？
    Store是Vuex仓库中用于存储状态的事务
    Vuex中有哪些属性？
    1.state 仓库初始化状态
    2.Getter Vuex的派生状态 （Vuex初始状态的计算属性）
    3.mutation 用于同步修改vuex的初始化状态（唯一）
    4.action 用于异步修改vuex的初始化状态 （还是调mutation来修改）
    4.moudule 用于模块化vuex
*/ 
import Vue from "vue";
import Vuex from "vuex";//引入vuex
// 注册vuex
Vue.use(Vuex);
// const声明变量 突出这个变量唯一性
const state = {
    name:"咱班kw19"
}
const getters = {
    // 仓库计算属性 vuex派生状态 不能修改vuex初始化状态色state值
    // 不带参数的派生状态
    webFrontEndkw19:state=>state.name+"的班长永远是大家好班长",
    // webFrontEndkw19:(state)=>{
    //     // console.log(state);//仓库初始化属性
    //     // 必须要return
    //     return state.name+"的班长永远是大家好班长"
    // },
    // webFront 带参数的派生状态
    webFrontEnd:state=>val=>state.name+val
}
var mutations = {
    // 修改vuex状态但是里面不能有异步
    // 定时器  axios promise 
    // 不带参
    changStorevalue(state){
        console.log(state);
        state.name = "金老师的弟子"
    },
    // 带参数的改变初始状态
    withTheCords(state, name){
        console.log(state);//初始化的状态
        console.log(name);//入得参数对象
        state.name = name.name;
    }
}
const actions = {
    // actions必须要写异步的
    // 面试题：在项目我们使用什么来改写修改vuex的状态 答：actions
    // 正常情况下我们修改vuex值之前都是调接口 所有都是使用actions
    // 改变state中的数据
    changeAsync(context){
        setTimeout(()=>{
            console.log(context)//当前的仓库对象
            context.commit("changStorevalue");
            // mutations唯一性修改state的值
        },1000)
    },
    //带参的actions 
    changeAsyncCon(context , load){
        setTimeout(()=>{
            console.log(context);
            console.log(load);
            context.commit({
                type:"withTheCords",
                name:load.name
            })
        },1000)
    }
}
// 抛出Vuex对象
// Store 仓库
// 
export default new Vuex.Store({
    state,//初始化变量
    getters,//计算属性
    mutations,//同步修改
    actions
})