// 自定义指令
// v-focus
import Vue from "vue";
// Vue.directive("指令名字","执行指令的函数")
Vue.directive("zhuqingqian" , function(el,bind){
    console.log(el)
    console.log(bind)
    el.style.fontSize ="100px";
    el.innerHTML = el.innerHTML+bind.value
})
// 初始化页面表单（el-input）自动获取焦点
Vue.directive("focus",{
    inserted(el,bind){
        // el-input 组件并不是立马渲染到页面 需要变成标签渲染页面 有时间的
        // 保证DOM渲染到页面后在执行该指令 inserted
        console.log(el)
        console.log(bind)
        el.firstElementChild.focus()
    }
})
// DragWindow 拖拽element的弹窗
Vue.directive("DragWindow" , {
    inserted(el , bind){
        let elDialogHeader = el.getElementsByClassName("el-dialog__header")[0];
        let elDialog = el.getElementsByClassName("el-dialog")[0];
        elDialogHeader.style.cursor ="move";
        // 获取弹窗的所有样式
        let style = elDialog.currentStyle || window.getComputedStyle(elDialog,null);
        elDialogHeader.onmousedown = (e)=>{
            let disX = e.clientX - elDialogHeader.offsetLeft;
            let disY  = e.clientY - elDialogHeader.offsetTop;
            let styL;
            let styT;
            // console.log(style)
            styL=+style.left.replace(/\px/g,'');
            styT=+style.top.replace(/\px/g,'');
            document.onmousemove = function(e){
                let l = e.clientX-disX;
                let t = e.clientY - disY;
                elDialog.style.left = `${l+styL}px`;
                elDialog.style.top = `${t + styT}px`;
            }
            document.onmouseup = function(){
                document.onmouseup = null;
                document.onmousemove = null;
            }
        }
    }
})