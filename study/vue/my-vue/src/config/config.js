// 项目的配置性文件
// config.js 项目中公共方法文件
// 缓存 
// Cookie 浏览器缓存 大小4kb 缓存中最小的 单个缓存存储时 单个存储的数据不能太长 如果太长存储无效 存储的数据 用户可以随意的清除
// localStorage  本地存储 存储于电脑硬盘文件中 手机内存卡中 大小非常大  清除方式 换内存条内存卡 电脑管家的清除 用户不能轻易删除 单个数据太长存在本地存储
// sessionStorage 会话存储 存储于当前窗口 大小很大 清除方法 关闭当前窗口 使用场景：关闭当前窗口就重新登录 那么项目中所有的缓存都用会话存储
//用户的缓存在获取时时不能保证用户一定能获取到 每种缓存用都有主动删除的方式 写代码时不能默认（我存了 缓存就一定存在）
// 缓存不在我们一般来说 用缓存要加三目和if判断 缓存不在时 我们常用的是 跳转到登录页 跳转到404
// js-cookie cookie插件 常用插件 
import Cookies from "js-cookie";
/*
    浏览器中所有的缓存（不管哪一个）是共享的  
    存储的名字必须要有特殊性和唯一性 yshop*** 后端的加密对于前端实行加密的存储
    存储的值 必须是基本数据类型 如果说遇到Obj Array function JSON.stringify()变成字符串
    存储的天数 （缓存的保质期 一般来说是7天 但是最终解释权归产品经理所有）
    隐藏的面试题（怎么解决Cookie的浏览器跨域）
    // ajax wx.request axios 请求后端会遇到跨域 服务器跨域
    // cookie是脚本注入 浏览器会抵触不正当的脚本 
    // 解决方案:1.我们使用的node+vue-cli搭建的服务器 所以不存在浏览器跨域
    //         2.如果我们不用node搭建服务器那么我在开发时会让JAVA帮我搭建一个Tomcat 阿帕奇服务器我在上面写代码
    //         3.如果后端不帮我搭建我会在度娘上下载一个Nginx来代理服务写代码
*/ 
// 存cookies 
// Cookie.set("存储的名字" ，存储的值 ,存储的天数)
export const SetCookie = (name , cookie)=>{//缓存的名字 存储的数据值
    Cookies.set(name , cookie ,{expires:7})
};
// 取缓存 Cookies.get(获取缓存的名字) 返回当前缓存的值 入参（需要获取的缓存名字）
export const GetCookie = name => Cookies.get(name);
// 删除缓存 Cookies.remove(需要删除的缓存名字)
export const RemoveCookie = name =>{
    Cookies.remove(name)
}
// localStorage 本地存储 存储量大 用户不能轻易删除 
// 当要缓存的数据单数据量比较大时 我们cookie存不下来时我们就是用本地存储
// 存缓存window.localStoage.setItem(存入的名字,存入的数据 （不能是引用数据类型）)
export const setStore = (name , obj)=>{
    window.localStorage.setItem(name , obj)
}

// 取缓存window.localStoage.getItem(要去的缓存的名字)
// export 和export default 抛出 把当前的vue或者js文件 进行模块化
export const getStore =name =>window.localStorage.getItem(name)

// 删除指定缓存
// window.localStoage.removeItem(删除的缓存的名字)
export const removeStore = name =>{
    window.localStorage.removeItem(name);
}
// 清除用户电脑里面所有的本地缓存 (场景：退出登录) window.localStoeage.clear()
export const getClear = ()=>{
    window.localStorage.clear()
}
// seeionStorage 会话缓存 （关闭窗口） 项目了Cookie和seeionStorage不要混用
// 存缓存window.sessionStorage.setItem() 
export const setSee = (name ,obj)=>{
    window.sessionStorage.setItem(name  , obj)
}
// 取缓存
export const getSee = name => window.sessionStorage.getItem(name)
// 删除缓存
export const removeSee = name => {
    window.sessionStorage.removeItem(name);
}

// setnullString 递归防null 
// 将后端返回的数据进行递归遇到null就变成{}
// 遇到"null" 就变成 ""
// resp 入参的后端数据
export const setnullString = (resp)=>{
    if(!resp){
        resp = {}
    }else{
        for(let i in resp){
            // {} []
            // 判断循环每一个值是不是对象或者数据但不是null
            if(resp[i]&& typeof(resp[i])=="object"){
                setnullString(resp[i])
            }else{
                // 基本数据类型 null
                if(Object.is(resp[i] , null)){
                    resp[i]= {}
                }else if(Object.is(resp[i],'null')){
                    resp[i]=""
                }
            }
        }
        return resp
    }

}