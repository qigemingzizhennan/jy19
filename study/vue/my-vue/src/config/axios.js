// 公司的axios必然走的是封装
import axios from "axios";
import {GetCookie, setnullString} from "../config/config";
// axios用来调接口的
// axios和ajax的区别axios是ajax的promise的封装
// .then()成功的回调  .catch()失败回调
// axios自带了拦截器 对于数据请求前的拦截和数据请求后的拦截
// request是axios的数据发送前的拦截器
axios.interceptors.request.use(config=>{
    console.log(config)//发送给后端的请求体
    // return 放行请求体发送给后端
    // Authorization
    // token令牌是判断用户有没有登录的标识
    // 用户没有token(登录) 返回404 
    // 如果用户已经登录成功 我们需要将TOKEN令牌放进请求头部 至于放的名字是啥后端定
    if(GetCookie("token")){
        // 用户已经登录
        config.headers["Authorization"] = GetCookie("token");
    }
    return config
})
// responese是axios的数据发送回来后的拦截器
axios.interceptors.response.use(res=>{
    console.log(res)//后端返回的数据
    // 
    if(res.status == 200 || res.status== 201){
        // 请求成功
        // 防null处理
        return setnullString(res.data)
    }else{
        // 请求失败
        // 跳转到500页面 => 按钮 返回登录页
        // 500 501 502 503 504 
        // 接口报错 服务宕机 断网
        
    }
})
// 抛出axios
export default axios