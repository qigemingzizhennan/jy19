// env 环境变量
/*
    一般来说项目服务器分为但不限于
    *JAVA自己的电脑搭建的服务器
    *公司搭建的给JAVA统一代码运行的开发服务
    *测试服务器 享有单独数据库
    *内测（灰度） 服务器
    *生产服务器（线上）
*/ 
// http://8.130.45.67:8000/auth/code
// development dev 开发环境
// productionUrl  master 线上环境
// test 测试
// http://8.130.45.67 IP地址  :8000 端口号 30 80公共端口 不安全
// 域名 www.baidu.com 
//  
let developmentUrl = "http://47.100.203.148:8000/";//地址
/*
    项目中后端除了服务器地址不一样之外
    不同而业务也不一样
    *正常增删改查的页面业务
    *第三方
    室内地图
    银行和支付宝微信支付
    身份证识别
    国家北斗定位 地图
    天气预报
    *上传 下载 （图片 文件）
*/ 
let urlDev = {
    businessUrl:`${developmentUrl}`,//公司
    thirdPartyUrl:"",//第三方
    uploadUrl:"",//上传
    downloadUrl:""//下载
}
let {businessUrl , thirdPartyUrl , uploadUrl ,downloadUrl } = urlDev;
// 抛出
console.log(businessUrl)
export {
    businessUrl,
    thirdPartyUrl,
    uploadUrl,
    downloadUrl
}