import Vue from 'vue'
import Router from 'vue-router'//路由 跳转页面 页面的传参 页面管理
import login from "@/view/login/login";//@ 根目录 src
// import home from "@/view/home/home";
// import control from "@/view/index/index";
// import HierarchicalTask from "@/view/member/HierarchicalTask/HierarchicalTask";//等级任务
// import leveMembership from "@/view/member/leveMembership/leveMembership";//会员等级
// import member from "@/view/member/member/member";//会员
// import commodityClassification from "@/view/shop/commodityClassification/commodityClassification";//商品分类
// import CommodityManagement from "@/view/shop/CommodityManagement/CommodityManagement";//管理商品
// 动态路由了解使用概念面试的时候能说出来就行 实际开发中往往会很复杂 （递归等操作所以一般不是你写）

Vue.use(Router)//use Vue中注册
let RouterMain = new Router({
  mode:"hash",//hash算法  Vue默认路由模式是hash
  // mode:"history",
  // hash history的区别是 hash的地址带# history不带#
  // mode 路由模式  ：hash路由 history路由（不带#） window.history 历史对象 里面有浏览历史的页面
      
  /*
      开始过程中我们用的是hash开发 但是上线时候我们就会在上线前将路由中的模式切换成history
      因为上线后 vue打包会变成html需要history模式才能跳转页面 但是在linux服务器部署的时候会导致history模式下
      页面空白 所以切换成history模式然后打包上线时候我们需要后端通过阿帕奇服务器重定向到第一个页面上
  */ 
   routes: [
    {
      path: '/',//当前路由的路径 "/" 路由默认的第一个打开的页面
      name: 'login',//当前路由的名字 
      component: login,//组件：当前路由的页面路径
      // 路由携带参数 让当前路由对象中携带我们自己需要的参数 进行传递
      meta:{
        title:"登录页"
      }
    }
    // ,{
    //   path:"/home",
    //   name:"home",
    //   component:home,
    //   // 二级路由设置
    //   redirect:"/control",//路由重定向 当前路由自动打开哪个下级路由
    //   children:[
    //     // 下级路由集合
    //     {
    //       path:"/control",
    //       name:"control",
    //       component:control,
    //       meta:{
    //         title:"首页"
    //       },
    //       // 路由中独享式路由守卫
    //       // 针对于项目中独特的页面的访问权限进行路由页面单独的拦截守卫
    //       beforeEnter:(to , from , next)=>{
    //         // 独享式在全局路由之后
    //         // beforeEnter在进入该页面之前触发
    //         console.log(to)//去哪个路由
    //         console.log(from)//来自哪个路由
    //         console.log(next)//放行
    //         next()
    //       }
    //     }
    //   ]
    // }
  ]
})

// 路由守卫 在vue页面跳转时安插守卫 在A页面跳转B页面时 跳转前触发守卫 跳转后也会触发守卫
// 全局路由守卫 
// beforeEach 跳转路由时跳转之前进行拦截和处理
RouterMain.beforeEach((to , from ,next)=>{
  console.log(to);//你到哪里去 路由对象
  console.log(from);//你从那里来 路由对象
  console.log(next)//函数 你可以通过了 放行
  // 1.如果to的路由对象不是登录注册等不会要登录的页面 && 用户没有登录就返回登录页
  // 2.如果用户已经登录但是用户to的页面的权限不满足我们就返回404页面
  // 3.如果from是null但是to是需要登录的页面 我们直接跳转到登录页
  // 4.如果用户是黑名单用户 to哪里都要去登录页或者是404 （黑名单 和 白名单）
  console.log(RouterMain)
  next()
})
// afterEach 该路由守卫已经发生了跳转后进入该页面的第一瞬间进行拦截守卫
RouterMain.afterEach((to , from)=>{
  console.log(to);
  console.log(from);
  // 进入页面先进行load的加载动画 然后再显示页面
  console.log(to.meta.title);
  document.title  = to.meta.title;
})
export default  RouterMain

