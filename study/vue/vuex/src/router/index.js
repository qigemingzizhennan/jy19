import Vue from 'vue'
import Router from 'vue-router'
// import HelloWorld from '@/components/HelloWorld'
import index from "@/views/index.vue";
import channelManage from "@/views/channelManage.vue";
Vue.use(Router)

export default new Router({
  routes: [
    {
      path:"/",
      name:"index",
      component:index
    },
    {
      path:"/channelManage",
      name:"channelManage",
      component:channelManage
    }
  ]
})
