import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);
let state= {
    delList:[
        {
            name:"头条",
            id:1
        },
        {
            name:"新闻",
            id:2
        },
        {
            name:"国内",
            id:3
        },
        {
            name:"国际",
            id:4
        }
    ],
    addlist:[
        {
            id:15,
            name:"开心"
        },
        {
            id:11,
            name:"不开心"
        },
        {
            id:12,
            name:"伤心"
        },
        {
            id:13,
            name:"不伤心"
        },
        {
            id:14,
            name:"难受"
        },
        {
            id:10,
            name:"不难"
        },
        {
            id:9,
            name:"体育"
        },
        {
            id:8,
            name:"娱乐"
        }
    ]
}
let mutations = {
    del(state,id){
        console.log(state);
        console.log(id.name);
        for(let index in state.delList){
            if(state.delList[index].id == id.name){
                state.addlist.push(state.delList[index])
                state.delList.splice(index ,1)
                
        }   
    }
    },
    add(state,id){
        for(let index in state.delList){
            if(state.addlist[index].id == id.name){
                state.delList.push(state.addlist[index])
                state.addlist.splice(index ,1)
            }   
        }
    }
}
let actions = {
    changeDel(context , load){
        // 调接口就调接口 不能调接口采用定时器
        setTimeout(()=>{
            context.commit({
                type:"del",
                name:load.name
            })
        },0)
    },
    changeadd(context , load){
        setTimeout(()=>{
            context.commit({
                type:"add",
                name:load.name
            })
        },0)
    }
}
export default new Vuex.Store({
    state,
    mutations,
    actions
})
