import axios from "axios";
import Mock from "mockjs";
 const list = Mock.mock({
    // 数据名的key值 | 数据随机出现的数量和范围
    "tableList|60-100":[//定义返回的是数组
        {
            // 每个对象的值
            image:"@image('200x100', '#ffcc33', '#FFF', 'png', 'Fast Mock')" ,
            id:"@id",
            name:"@csentence",
            time:"@datetime",
            tname:"@csentence(3, 5)"
        }
    ]
})
// data => 一页多少条  当前第一页
/*
    pageNum 第几页
    pageSize 一页几条
*/ 
export const listTab = (data)=>axios({
    url:"/Nbkw",
    method:"post",
    data
})
// Mock.mock(正则的 url地址, 处理函数（请求体）={})
Mock.mock(/\/Nbkw/, config=>{
    // console.log(config)
    let data = JSON.parse(config.body);
    let {pageNum , pageSize} = data;
    /*
        total:当前有多少条
        对应的数据
    */ 
   let total = list.tableList.length;
   let _list = list.tableList;
    //对应的数据
    /*
    pageNum 第几页
    pageSize 每页几条
*/ 
   let user = _list.filter((item ,index)=>index<pageSize*pageNum&&index>=pageSize*(pageNum-1))
//    console.log(user)
   return {
    // 后端返回的数据
    data:{
        list:user,
        total,
        code:200
    }
   }
})
// console.log(list);