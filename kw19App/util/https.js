// 小程序里面https请求  -- 本地可以用http || IP 不校验合法域名 线上情况必须用https
// App里面真机测试 必须使用的就是https 非手机情况
let config = ` https://api-hmugo-web.itheima.net`;//开发环境 生产环境 测试环境 灰度环境 
 /*let config = ` https://api-hmugo-web.itheima.net`;*/
 // let config = ` https://api-hmugo-web.itheima.net`;
 // let config = ` https://api-hmugo-web.itheima.net`;
 // let config = ` https://api-hmugo-web.itheima.net`;
// https://api-hmugo-web.itheima.net/api/public/v1/goods/search
let business = {
	common:`/api/public/v1/`,//本地地址 
	// thirdPary:"" 第三方
	// upload: 上传下载
}
let urlcom = config+business.common;//普通地址
function request(
	url,
	data={},
	method="get",
	doSuccess,
	dofail,
	isToken = false
){
	uni.request({
		url:urlcom+url,//后端地址
		data,//传入后端的数据
		header:{},
		method,
		success:(res)=>{
			// 成功的回调
			doSuccess(res)
		},
		fail(err) {
			dofail(err)
		}
	})
}
module.exports = {request}